import json
import re

class RLEParser(object):
    def __init__(self, text):
        self.text = text

    @classmethod
    def from_file(cls, filepath):
        with open(filepath, 'r') as rle_file:
            rle = rle_file.read()
        return cls(rle)
    
    @property
    def cleaned_text(self):
        def _is_main_content(line):
            is_not_main_content = (
                line.startswith('#C') or
                line.startswith('#c') or
                line.startswith('#N') or
                line.startswith('#O') or
                line.startswith('#P') or
                line.startswith('#R') or
                line.startswith('#r') or
                line.startswith('x')
            )
            return (not is_not_main_content)
            
        cleaned_text = ''
        for line in self.text.splitlines():
            if _is_main_content(line):
                cleaned_text += line
        return cleaned_text
    
    @property
    def name(self):
        matches_N = re.findall('#N(.*)', self.text)
        if not len(matches_N):
            return ''
        name = matches_N[0].strip()
        return name
    
    @property
    def width(self):
        matches_x = re.findall('x *= *([0-9]*)', self.text)
        if not len(matches_x):
            return None
        width = int(matches_x[0])
        return width

    @property
    def height(self):
        matches_y = re.findall('y *= *([0-9]*)', self.text)
        if not len(matches_y):
            return None
        height = int(matches_y[0])
        return height

    @property
    def alive_list(self):
        alive_list = list()
        number_of_skipped_lines = 0
        for id_line, line in enumerate(self.cleaned_text.split('$')):
            matches_cells = re.findall('(\d*)(b|o)', line)
            if not matches_cells:
                number_of_skipped_lines +=1
                continue
            cells_flattened = self._flatten(matches_cells)
            y = id_line-number_of_skipped_lines
            alive_list.extend(
                [(id_cell, y) for id_cell, m in enumerate(cells_flattened) if m=='o']
            )
        return alive_list
    
    @property
    def json(self):
        rle_as_dict = {'name': self.name, 'alive_list': self.alive_list}
        rle_as_json = json.dumps(rle_as_dict)
        return rle_as_json

    @property
    def alive_list_json(self):
        alive_list_json = json.dumps(self.alive_list)
        return alive_list_json

    @staticmethod
    def _flatten(matches):
        matches_with_ones = [(a,b) if a!='' else ('1',b) for a,b in matches]
        cells = [(cell_content,)*int(cell_multiplicator) for cell_multiplicator, cell_content in matches_with_ones]
        cells_flattened = [_ for cell_tuple in cells for _ in cell_tuple]
        return cells_flattened


