from setuptools import setup

setup(
    name='conway',
    version='0.0.1',
    description='''
        An implementation of Conway's game of life including an rle parser
    ''',
    author='Benjamin Becker',
    author_email='kontakt@benjaminbecker.net',
    packages=['game_of_life', 'rle'],  # same as name
    install_requires=[],  # external packages as dependencies
)
