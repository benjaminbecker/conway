import unittest
from .context import game_of_life
from game_of_life.life import Life

class Test_Life(unittest.TestCase):
    def test_it_returns_alive_list_json(self):
        l = Life(20, 20, [(1,2), (3,4), (5,6)])
        alive_list_json = l.alive_list_json
        self.assertEqual(
            alive_list_json,
            '[[1, 2], [3, 4], [5, 6]]'
        )
