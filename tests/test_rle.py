import unittest
from .context import rle
from rle.parser import RLEParser

class Test_rle(unittest.TestCase):
    def setUp(self):
        sample_text = '''
        #N Gosper glider gun
        #C This was the first gun discovered.
        #C As its name suggests, it was discovered by Bill Gosper.
        x = 36, y = 9, rule = B3/S23
        24bo$22bobo$12b2o6b2o12b2o$11bo3bo4b2o12b2o$2o8bo5bo3b2o$2o8bo3bob2o4b
        obo$10bo5bo7bo$11bo3bo$12b2o!
        '''
        self.parser = RLEParser(sample_text)
    
    def test_it_reads_width(self):
        width = self.parser.width
        self.assertEqual(width, 36)
    
    def test_it_reads_width_without_space_before_equal(self):
        width = RLEParser('x=  15').width
        self.assertEqual(width, 15)
    
    def test_it_reads_width_without_space_after_equal(self):
        width = RLEParser('x  =15').width
        self.assertEqual(width, 15)
    
    def test_it_returns_None_for_width_if_no_width(self):
        width = RLEParser('').width
        self.assertEqual(width, None)
    
    def test_it_reads_height(self):
        height = self.parser.height
        self.assertEqual(height, 9)
    
    def test_it_returns_None_for_height_if_no_height(self):
        height = RLEParser('').height
        self.assertEqual(height, None)
    
    def test_it_reads_bobo(self):
        alive_list = RLEParser('x=4,y=1\nbobo').alive_list
        self.assertEqual(alive_list, [(1,0), (3,0)])
    
    def test_it_reads_bobo_bobo(self):
        alive_list = RLEParser('x=4,y=1\nbobo$\nbobo').alive_list
        self.assertEqual(alive_list, [(1,0), (3,0), (1,1), (3,1)])
    
    def test_it_reads_2b3o2bo(self):
        alive_list = RLEParser('x=4,y=1\n2b3o2bo$').alive_list
        self.assertEqual(alive_list, [(2,0), (3,0), (4,0), (7,0)])

    def test_it_reads_2b3oo2bbo(self):
        alive_list = RLEParser('x=4,y=1\n2b3oo2bbo$').alive_list
        self.assertEqual(alive_list, [(2,0), (3,0), (4,0), (5,0), (9,0)])
    
    def test_cleaned_text_does_not_contain_line_with_x(self):
        parser = RLEParser('x = 10, y = 20\nContent of file')
        self.assertEqual(parser.cleaned_text, 'Content of file')

    def test_cleaned_text_does_not_contain_pattern_name(self):
        parser = RLEParser('#N Name of pattern\nContent of file')
        self.assertEqual(parser.cleaned_text, 'Content of file')

    def test_cleaned_text_does_not_contain_comments(self):
        parser = RLEParser('#C A comment\nContent of file')
        self.assertEqual(parser.cleaned_text, 'Content of file')
    
    def test_cleaned_text_does_not_contain_author_info(self):
        parser = RLEParser('#O Author info\nContent of file')
        self.assertEqual(parser.cleaned_text, 'Content of file')
    
    def test_cleaned_text_does_not_contain_lines_beginning_with_hash_P_R_r(self):
        parser = RLEParser('#P Something\#R Something\n#r Something\nContent of file')
        self.assertEqual(parser.cleaned_text, 'Content of file')
    
    def test_it_returns_name(self):
        name = self.parser.name
        expected_name = 'Gosper glider gun'
        self.assertEqual(name, expected_name)
    
    def test_it_returns_empty_string_if_no_name_in_rle(self):
        parser = RLEParser('#P Something\#R Something\n#r Something\nContent of file')
        self.assertEqual(parser.name, '')
    
    def test_json_returns_name_and_alive_list(self):
        sample_text = '#N test name\n#C Some more text\n#C Even more text\nx=4,y=1\nbobo'
        parser = RLEParser(sample_text)
        expected_json = '{"name": "test name", "alive_list": [[1, 0], [3, 0]]}'
        actual_json = parser.json
        self.assertEqual(actual_json, expected_json)
