#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from itertools import product
from rle.parser import RLEParser
import json

class Life():
    def __init__(self, N, M, alive_list, sleep_time=0.5, n_rounds=1000):
        self.N = N
        self.M = M
        self.alive_list = alive_list
        self.sleep_time = sleep_time
        self.N_ROUNDS = n_rounds
    
    @classmethod
    def from_rle_file(cls, filepath):
        parser = RLEParser.from_file(filepath)
        alive_list = parser.alive_list
        N = parser.width
        M = parser.height
        return cls(N, M, alive_list)
    
    @classmethod
    def blinker_factory(cls):
        N = 80
        M = 40
        alive_list = [(19,20), (20,20), (21,20)]
        sleep_time = 1
        n_rounds = 20
        return cls(N, M, alive_list, sleep_time, n_rounds)

    @classmethod
    def pentomino_factory(cls):
        N = 160
        M = 40
        alive_list = [(41,20), (42,20), (40,21), (41,21), (41,22)]
        sleep_time = 0.1
        n_rounds = 2000
        return cls(N, M, alive_list, sleep_time, n_rounds)
    
    @classmethod
    def glider_factory(cls):
        N = 80
        M = 40
        alive_list = [
            (41,20), (42,20), (43,20), (44,20), (45,20),
            (40,21), (45,21), 
            (45,22),
            (40,23), (44,23)
        ]
        sleep_time = 0.1
        n_rounds = 2000
        return cls(N, M, alive_list, sleep_time, n_rounds)

    @classmethod
    def small_gun_factory(cls):
        N=160
        M=40
        alive_list = [
            (1,6),
            (3,5), (3,6),
            (5,2), (5,3), (5,4),
            (7,1), (7,2), (7,3),
            (8,2)
        ]
        alive_list = [(x+80, y+20) for (x,y) in alive_list]
        sleep_time = 1
        n_rounds = 2000
        return cls(N, M, alive_list, sleep_time, n_rounds)

    @classmethod
    def gosper_glider_gun_factory(cls):
        N=160
        M=40
        alive_list = cls._gosper_glider_gun()
        alive_list = [(x, y) for (x,y) in alive_list]
        sleep_time = 0.2
        n_rounds = 2000
        return cls(N, M, alive_list, sleep_time, n_rounds)

    @classmethod
    def double_gosper_glider_gun_factory(cls):
        N=160
        M=40
        alive_list = cls._gosper_glider_gun()
        alive_list = [(x, y) for (x,y) in alive_list]
        alive_list += [(80-x, y) for (x,y) in alive_list]
        sleep_time = 0.2
        n_rounds = 2000
        return cls(N, M, alive_list, sleep_time, n_rounds)

    @staticmethod
    def _gosper_glider_gun():
        alive_list = [
            (2,6), (3,6), (2,7), (3,7),
            (12,6), (12,7), (12,8),
            (13,5), (13,9),
            (14,4), (14,10),
            (15,4), (15,10),
            (16,7),
            (17,5), (17,9),
            (18,6), (18,7), (18,8),
            (19,7),
            (22,4), (22,5), (22,6),
            (23,4), (23,5), (23,6),
            (24,3), (24,7),
            (26,2), (26,3), (26,7), (26,8),
            (36,4), (36,5), (37,4), (37,5), 
        ]
        return alive_list

    @property
    def alive_list_json(self):
        alive_list_json = json.dumps(self.alive_list)
        return alive_list_json
    
    def is_dead(self, x, y):
        return (x, y) not in self.alive_list

    def is_alive(self, x, y):
        return (x, y) in self.alive_list

    def _count_neighbours(self, x, y):
        neighbour_cells = ((x,y-1), (x+1,y-1), (x+1,y), (x+1,y+1), (x,y+1), (x-1,y+1), (x-1,y), (x-1,y-1))
        n_neighbours = sum(
            (self.is_alive(*neighbour) for neighbour in neighbour_cells)
        )
        return n_neighbours

    def has_three_neighbours(self, x, y):
        return (self._count_neighbours(x, y) == 3)
    
    def has_less_than_two_neighbours(self, x, y):
        return (self._count_neighbours(x, y) < 2)

    def has_two_or_three_neighbours(self, x, y):
        n_neighbours = self._count_neighbours(x, y)
        return (n_neighbours==2) or (n_neighbours==3)

    def has_more_than_three_neighbours(self, x, y):
        return (self._count_neighbours(x, y) > 3)

    def awaken(self, x, y):
        self.alive_list_new.append((x, y))

    def kill(self, x, y):
        pass

    def keep_alive(self, x, y):
        self.alive_list_new.append((x, y))

    def develop(self):
        self.alive_list_new = []
        for (x, y) in product(range(self.N), range(self.M)):
            if self.is_dead(x, y) and self.has_three_neighbours(x, y):
                self.awaken(x, y)
                continue
            if self.is_alive(x, y):
                if self.has_less_than_two_neighbours(x, y):
                    self.kill(x, y)
                    continue
                if self.has_two_or_three_neighbours(x, y):
                    self.keep_alive(x, y)
                    continue
                if self.has_more_than_three_neighbours(x, y):
                    self.kill(x, y)
                    continue
        self.alive_list = self.alive_list_new

    def draw(self):
        output = ''.join('-'*(self.N+2))
        output += '\n'
        for y in range(self.M):
            output += '|'
            for x in range(self.N):
                if (x, y) in self.alive_list:
                    output += '█'
                else:
                    output += ' '
            output += '|\n'
        output += ''.join('-'*(self.N+2))
        output += '\n'
        print(output)
    
    def run(self):
        for __ in range(self.N_ROUNDS):
            self.draw()
            time.sleep(self.sleep_time)
            self.develop()
    
if __name__ == "__main__":
    game = Life.from_rle_file('./rle_samples/gosper_glider_gun.rle')
    game.alive_list = [(x+50,y+20) for (x,y) in game.alive_list]
    game.N = 100
    game.M = 60
    game.run()

